using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public static GameManager SharedInstance;
    private bool gameOver = false;
    [SerializeField] GameObject gameOverScreen;
    [SerializeField] GameObject winScreen;
    [SerializeField] TextMeshProUGUI countdownText;
    [SerializeField] TextMeshProUGUI energyText;

    public bool isUsingTimer; //Tell if this level goal is time or enemy number;
    private float levelDuration;
    public int enemyNumber;

    public int energy;
    private int startEnergy;
    [SerializeField] float energyPassiveTimer = 10.0f;
    [SerializeField] int energyPassiveAmount = 0;

    public struct MinMax {
        public float min;
        public float max;
        public MinMax(float min, float max) {
            if (min > max)
            {
                this.min= max;
                this.max = min;
            }
            else
            {
                this.min = min;
                this.max = max;
            }
        }
    }
    public MinMax spawnIntervals;

    private void Awake() 
    {
        if(GameManager.SharedInstance == null)
        {
            SharedInstance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        
        LoadLevelDifficulty();
    }

    void LoadLevelDifficulty()
    {
        switch(MainManager.SharedInstance.difficulty)
        {
            case 0:
                startEnergy = 40;
                isUsingTimer = false;
                enemyNumber = 10;
                spawnIntervals = new MinMax(1.2f, 4.0f);
            break;
            case 1:
                startEnergy = 50;
                isUsingTimer = false;
                enemyNumber = 20;
                spawnIntervals = new MinMax(0.8f, 3.0f);
            break;
            case 2:
                startEnergy = 70;
                isUsingTimer = true;
                levelDuration = 30;
                spawnIntervals = new MinMax(0.4f, 2.0f);
            break;
            default:
                startEnergy = 120;
                isUsingTimer = false;
                enemyNumber = 10;
                spawnIntervals = new MinMax(1.2f, 4.0f);
            break;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        LoadReferences();
        SetUpGameLevel();

        StartCoroutine(EnergyPassiveIncome());
       
    }

    void LoadReferences()
    {
        if (gameOverScreen == null)
        {
            gameOverScreen = GameObject.Find("Game Over Screen");
            gameOverScreen.SetActive(false);
        }
        if (winScreen == null)
        {
            winScreen = GameObject.Find("Win Screen");
            winScreen.SetActive(false);
        }
        if (countdownText == null)
        {
            countdownText = GameObject.Find("Countdown Text").GetComponent<TextMeshProUGUI>();
        }
        if (energyText == null)
        {
            energyText = GameObject.Find("Energy Text").GetComponent<TextMeshProUGUI>();
        }
    }

    void SetUpGameLevel()
    {
        if (!isUsingTimer)
        {
            levelDuration = enemyNumber;
            countdownText.text = "" + Mathf.CeilToInt(levelDuration);
        }
        else
        {
            countdownText.text = ("" + Mathf.CeilToInt(levelDuration));
            StartCoroutine(LevelTimer());
        }
        
        energy = startEnergy;
        UpdateEnergyText(0);
    }

    private void LateUpdate() {
        if (levelDuration <= 0 && !gameOver)
        {
            LevelClear();
        }
    }

    public void SetGameOverState(bool newState)
    {
        gameOver = newState;

        if (gameOver)
        {
            SpawnEnemies.SharedInstance.StopSpawn();
            gameOverScreen.SetActive(true);
        }
        else
        {
            SpawnEnemies.SharedInstance.StartSpawn();
            gameOverScreen.SetActive(false);
        }
    }

    public void LevelClear()
    {
        gameOver = true;
        SpawnEnemies.SharedInstance.StopSpawn();
        winScreen.SetActive(true);
    }

    public void UpdateEnergyText(int energyToAdd)
    {
        energy += energyToAdd;
        energyText.text = ("Energy: " + energy);
    }

    public void UpdateCountDownText(int countToAdd)
    {
        levelDuration += countToAdd;
        countdownText.text = ("" + Mathf.CeilToInt(levelDuration));
    }
    public void UpdateCountDownText(string newText)
    {
        countdownText.text = newText;
    }

    IEnumerator EnergyPassiveIncome()
    {
        while(!gameOver)
        {
            yield return new WaitForSeconds(energyPassiveTimer);
            UpdateEnergyText(energyPassiveAmount);
        }
    }

    IEnumerator LevelTimer()
    {
        while(levelDuration > 0)
        {
            if(gameOver)
                yield break;
            
            yield return new WaitForSeconds(1);
            UpdateCountDownText(-1);
        }
    }
}
