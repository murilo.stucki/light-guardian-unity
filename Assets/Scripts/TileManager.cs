using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TileManager : MonoBehaviour
{
    private GameObject gridObject;

    public Color highlightColor;
    public Color normalColor;

    public GameObject obstacle;
    [SerializeField] ParticleSystem smokeParticle;
    [SerializeField] GameObject bonusEnergyText;
    
    private AudioSource audioSource;
    [SerializeField] AudioClip enemyDeathSound;

    //Own Coordinates
    private Vector2Int index;
    // ENCAPSULATION
    public Vector2Int Index 
    {
        get {return index; }
        set { index = value;}
    }

    // Start is called before the first frame update
    void Start()
    {
        gridObject = transform.parent.gameObject;
        bonusEnergyText.SetActive(false);
        audioSource = GetComponent<AudioSource>();

        if (obstacle == null)
        {
            obstacle = transform.GetChild(0).gameObject;
        }
        obstacle.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseOver() 
    {
        SpawnTiles.SharedInstance.HighlightTiles(index, highlightColor, true);
    }

    private void OnMouseExit() {
        SpawnTiles.SharedInstance.HighlightTiles(index, normalColor, false);
    }

    private void OnMouseDown() 
    {
        SpawnTiles.SharedInstance.SetObstaclesActive(index);
    }

    public void SetTileIndex(Vector2Int newIndex)
    {
        index = newIndex;
    }

    public void DestroyObstacle(int energyBonus)
    {
        PlaySmokeParticle();
        audioSource.PlayOneShot(enemyDeathSound);
        StartCoroutine(ShowBonusEnergy(energyBonus));
        GameManager.SharedInstance.UpdateEnergyText(energyBonus);
        obstacle.SetActive(false);
    }

    public void PlaySmokeParticle()
    {
        smokeParticle.Play();
    }

    IEnumerator ShowBonusEnergy(int value)
    {
        bonusEnergyText.transform.position = transform.position + new Vector3(0, 2f, 1f);
        bonusEnergyText.SetActive(true);
        bonusEnergyText.GetComponent<TextMeshPro>().text = ("+" + value);
        yield return new WaitForSeconds(1);
        bonusEnergyText.SetActive(false);
    }
}
