using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class MenuUIHandler : MonoBehaviour
{

    public GameObject startContainer;
    public GameObject levelSelectContainer;

    private AudioSource audioSource;
    [SerializeField] AudioClip clickSound;

    private void Start() 
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    public void StartNew()
    {
        SceneManager.LoadScene(1);
    }

    public void EasyButton()
    {
        PlayClickSound();
        MainManager.SharedInstance.difficulty = 0;
        StartNew();
    }
    public void MediumButton()
    {
        PlayClickSound();
        MainManager.SharedInstance.difficulty = 1;
        StartNew();
    }
    public void HardButton()
    {
        PlayClickSound();
        MainManager.SharedInstance.difficulty = 2;
        StartNew();
    }

    public void StartButton()
    {
        PlayClickSound();
        startContainer.SetActive(false);
        levelSelectContainer.SetActive(true);
    }

    public void Exit()
    {
        PlayClickSound();
        #if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
        #else
        Application.Quit(); // original code to quit Unity player
        #endif
    }

    public void PlayClickSound()
    {
        audioSource.PlayOneShot(clickSound);
    }
}
