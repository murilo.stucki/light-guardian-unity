using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldenEnemy : Enemy // INHERITANCE
{
    private AudioSource audioSource;
    [SerializeField] AudioClip uniqueSound;

    private void Start() 
    {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(PlayUniqueSound());
    }

    protected override void HandleObstacleHit(Collider obstacle) // POLYMORPHISM
    {
            obstacle.transform.GetComponentInParent<TileManager>().DestroyObstacle(energyReward);

            if (!GameManager.SharedInstance.isUsingTimer)
            {
                GameManager.SharedInstance.UpdateCountDownText(-1);
            }
            
            gameObject.SetActive(false);
    }

    IEnumerator PlayUniqueSound()
    {
        yield return new WaitForSeconds(1);
        audioSource.PlayOneShot(uniqueSound);
    }
}
