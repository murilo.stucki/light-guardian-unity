using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{

    public static SpawnEnemies SharedInstance;

    private ObjectPool enemyPool;
    private int gridXSize;
    private float maxSpawnInterval = 4;
    private float minSpawnInterval = 1;
    private bool spawnStopped = false;

    private void Awake() 
    {
        if(SpawnEnemies.SharedInstance == null)
        {
            SharedInstance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        
    }
    // Start is called before the first frame update
    void Start()
    {
        enemyPool = GetComponent<ObjectPool>();

        gridXSize = SpawnTiles.SharedInstance.gridSize.x;
        transform.position = new Vector3((3 - (gridXSize / 2)) * 3, 0, 22.0f);
        minSpawnInterval = GameManager.SharedInstance.spawnIntervals.min;
        maxSpawnInterval = GameManager.SharedInstance.spawnIntervals.max;

        if (GameManager.SharedInstance.isUsingTimer)
        {
            StartCoroutine(RandomSpawner());
        }
        else
        {
            StartCoroutine(RandomSpawner(GameManager.SharedInstance.enemyNumber));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnEnemy()
    {
        float xPosition = (3 + SpawnTiles.SharedInstance.tileSpacing) * Random.Range(0, gridXSize);
        Vector3 spawnPosition = new Vector3(transform.position.x + xPosition, transform.position.y, transform.position.z);
        GameObject enemy = enemyPool.GetPooledObject();
        if (enemy != null)
        {
            enemy.transform.position = spawnPosition;
            enemy.transform.rotation = transform.rotation;
            enemy.SetActive(true);
        }
        //Instantiate(enemyPrefab, spawnPosition, transform.rotation, transform);
    }

    IEnumerator RandomSpawner()
    {
        while(!spawnStopped)
        {
            float interval = Random.Range(minSpawnInterval, maxSpawnInterval);
            yield return new WaitForSeconds(interval);
            if (!spawnStopped)
                SpawnEnemy();
        }
        
    }
    IEnumerator RandomSpawner(int number) // POLYMORPHISM overloading
    {
        while(!spawnStopped && number > 0)
        {
            float interval = Random.Range(minSpawnInterval, maxSpawnInterval);
            yield return new WaitForSeconds(interval);
            if (!spawnStopped)
            {
                SpawnEnemy();
                number--;
            }
        }
        
    }

    public void StopSpawn()
    {
        spawnStopped = true;
        enemyPool.DeactivateObjects();
        // GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        // for (int i = 0; i < enemies.Length; i++)
        //     Destroy(enemies[i]);
        //gameObject.SetActive(false);
    }
    public void StartSpawn()
    {
        spawnStopped = false;
        gameObject.SetActive(true);
    }
}
