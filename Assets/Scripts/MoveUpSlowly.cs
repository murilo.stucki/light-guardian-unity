using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUpSlowly : MonoBehaviour
{
    [SerializeField] float speed = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (gameObject.activeInHierarchy)
            transform.Translate(Vector3.up * speed * Time.deltaTime);
    }
}
