# Light Guardian

A game prototype developed using Unity3D, by Murilo Luz.

The player goal is to stop the creatures of the night from reaching the town, and the way to do that is to keep the lanterns in the city entrance always lit.
The enemies can't move where there is light, so they will try to extinguish all the lights and attack your town.

## Installation

Download Unity Hub and Unity Editor version 2020.3.26f1.
Download all files and unzip them. 
Open Unity Hub and click Open project.
Select the unzipped folder and the correct Unity version.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](/LICENSE)
